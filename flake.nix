{
  description = "PDF annotation";
  inputs = {
    utils.url = "github:numtide/flake-utils";
    crate2nix = {
      url = "github:balsoft/crate2nix/tools-nix-version-comparison";
      flake = false;
    };
  };
  outputs = { self, nixpkgs, utils, crate2nix }:
    utils.lib.eachDefaultSystem (system:
      let
        name = "pdf-annotate";
        pkgs = import nixpkgs { inherit system; };

        # Configuration for the non-Rust dependencies
        buildInputs = with pkgs; [ ];
        nativeBuildInputs = with pkgs; [ ];
      in rec {

        # `nix develop`
        devShell =
          pkgs.mkShell { buildInputs = with pkgs; [ nodejs watchexec reuse ]; };
      });
}
