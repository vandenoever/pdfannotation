INPUTS = $(shell git ls-tree -r --name-only HEAD)

dist/index.html: ${INPUTS} src/pdf-annotation.ts node_modules
	npx vue-tsc --noEmit
	npx vite build

src/pdf-annotation.ts: pdf-annotation.schema.json node_modules
	npx quicktype --nice-property-names --raw-type any --acronym-style camel -s schema $< -o $@
	npx prettier --write $@

node_modules:
	npm ci

clean:
	rm -r src/pdf-annotation.ts dist
