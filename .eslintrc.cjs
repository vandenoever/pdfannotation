module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: true,
  },
  extends: [
    "eslint:recommended",
    "plugin:vue/vue3-recommended",
    "@vue/typescript/recommended",
    "@vue/prettier",
  ],
  parserOptions: {
    ecmaVersion: 13,
    sourceType: "module",
    parser: "@typescript-eslint/parser",
  },
  plugins: ["vue", "html", "prettier", "@typescript-eslint"],
  rules: {
    // 'vue/no-unused-vars': 'error'
    "prettier/prettier": "error",
  },
};
