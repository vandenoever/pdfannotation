// SPDX-FileCopyrightText: 2022 Jos van den Oever
//
// SPDX-License-Identifier: AGPL-3.0-only

import { createApp } from "vue";
import { createPinia } from "pinia";
import "./style.css";
import App from "./App.vue";
const app = createApp(App);
app.use(createPinia());
app.mount("#app");
