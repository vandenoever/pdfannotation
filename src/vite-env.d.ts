// SPDX-FileCopyrightText: 2022 Jos van den Oever
//
// SPDX-License-Identifier: AGPL-3.0-only

/// <reference types="vite/client" />

declare module "*.vue" {
  import type { DefineComponent } from "vue";
  const component: DefineComponent<{}, {}, any>;
  export default component;
}

declare module "pdfjs-dist/legacy/build/pdf.worker.entry";
declare module "pdfjs-dist/build/pdf";
declare module "pdfjs-dist/build/pdf.worker.entry";
