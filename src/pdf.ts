// SPDX-FileCopyrightText: 2022 Jos van den Oever
//
// SPDX-License-Identifier: AGPL-3.0-only

import { getDocument, PDFDocumentProxy } from "pdfjs-dist";
import { GlobalWorkerOptions } from "pdfjs-dist/build/pdf";
import PDFJSWorker from "pdfjs-dist/build/pdf.worker.entry";
import { TextItem, TextMarkedContent } from "pdfjs-dist/types/src/display/api";
import { EventBus, PDFPageView } from "pdfjs-dist/web/pdf_viewer";
import * as pdfjsViewer from "pdfjs-dist/web/pdf_viewer";
import { Annotation } from "./Annotation";
import { RefinedBy, RefinedByType } from "./pdf-annotation";

GlobalWorkerOptions.workerSrc = PDFJSWorker;

export type Pdf = {
  eventBus: EventBus;
  doc: PDFDocumentProxy;
  pageView: PDFPageView;
  pageNumber: number;
};

export async function loadPdf(
  container: HTMLDivElement,
  url: string,
  pageNumber: number
): Promise<Pdf> {
  const eventBus = new pdfjsViewer.EventBus();
  const loadingTask = getDocument({ url });
  const doc = await loadingTask.promise;
  const page = await doc.getPage(pageNumber);
  const scale = 1;
  const viewport = page.getViewport({ scale });
  // Support HiDPI-screens.
  const outputScale = window.devicePixelRatio || 1;
  const pageView = new pdfjsViewer.PDFPageView({
    container,
    id: pageNumber,
    scale,
    defaultViewport: page.getViewport({ scale }),
    eventBus,
    // We can enable text/annotation/xfa/struct-layers, as needed.
    textLayerFactory: !doc.isPureXfa
      ? new pdfjsViewer.DefaultTextLayerFactory()
      : undefined,
    annotationLayerFactory: new pdfjsViewer.DefaultAnnotationLayerFactory(),
    xfaLayerFactory: doc.isPureXfa
      ? new pdfjsViewer.DefaultXfaLayerFactory()
      : undefined,
    structTreeLayerFactory: new pdfjsViewer.DefaultStructTreeLayerFactory(),
  });
  pageView.setPdfPage(page);
  await pageView.draw();
  return {
    eventBus,
    doc,
    pageView,
    pageNumber,
  };
}

export async function setPage(pdf: Pdf, pageToView: number) {
  if (pdf.pageNumber === pageToView) {
    return;
  }
  pdf.pageNumber = pageToView;
  const page = await pdf.doc.getPage(pageToView);
  pdf.pageView.setPdfPage(page);
  await pdf.pageView.draw();
}

export type SelectionTexts = {
  id: string; // unique id within a pdf
  y: number; // smallest y in the rectangles
  startOffset: number;
  endOffset: number;
  rectStyles: string[];
  texts: string[];
};

// cast to Text if it is a Text and if it is a child of ancestor
function asTextContainedBy(child: Node, ancestor: Node): Text | undefined {
  if (child.nodeType !== Node.TEXT_NODE) {
    return;
  }
  const pos = ancestor.compareDocumentPosition(child);
  if (pos & Node.DOCUMENT_POSITION_CONTAINED_BY) {
    return child as Text;
  }
}

type AddTexts = {
  texts: Text[];
  textStrings: string[];
  nodeCount: number;
};

function getTexts(div: HTMLDivElement, start?: Node, end?: Node): AddTexts {
  let nodeCount = 0;
  const texts: Text[] = [];
  const textStrings: string[] = [];
  const walker = document.createTreeWalker(
    div,
    NodeFilter.SHOW_TEXT | NodeFilter.SHOW_ELEMENT
  );
  let node = walker.nextNode();
  let add = !start;
  while (node) {
    if (node === start) {
      add = true;
    }
    if (add) {
      if (node.nodeType === 3) {
        const text = node as Text;
        if (add) {
          texts.push(text);
          textStrings.push(text.data);
        }
        if (text === end) {
          break;
        }
      } else if (node.nodeType === 1) {
        const element = node as Element;
        if (element.nodeName === "BR") {
          textStrings[textStrings.length - 1] += "\n";
        }
      }
    }
    nodeCount += 1;
    node = walker.nextNode();
  }
  return {
    texts,
    textStrings,
    nodeCount,
  };
}

function getSelectedText(
  pageNumber: number,
  div: HTMLDivElement
): SelectionTexts | undefined {
  const selection = window.getSelection();
  if (!(selection && selection.rangeCount === 1)) {
    return;
  }
  const range = selection.getRangeAt(0);
  const start = asTextContainedBy(range.startContainer, div);
  const end = asTextContainedBy(range.endContainer, div);
  if (!start || !end) {
    return;
  }
  const { texts, textStrings, nodeCount } = getTexts(div, start, end);
  if (!texts) {
    return;
  }
  const { exact } = getTextQuote(
    textStrings,
    range.startOffset,
    range.endOffset
  );
  const id = pageNumber + " " + nodeCount + " " + exact;
  return createSelectionTexts(id, div, range, textStrings);
}

export function getTextQuote(
  texts: string[],
  startOffset: number,
  endOffset: number
): RefinedBy {
  const start = texts[0];
  const end = texts[texts.length - 1];
  let text;
  if (texts.length === 1) {
    text = start.substring(startOffset, endOffset);
  } else {
    text = start.substring(startOffset);
    for (let i = 1; i < texts.length - 1; ++i) {
      text += texts[i];
    }
    text += end.substring(0, endOffset);
  }
  return {
    type: RefinedByType.TextQuoteSelector,
    exact: text,
    prefix: start.substring(0, startOffset) || undefined,
    suffix: end.substring(endOffset) || undefined,
  };
}

function style(box: DOMRect) {
  const s =
    "top:" +
    box.y +
    "px;left:" +
    box.x +
    "px;width:" +
    box.width +
    "px;height:" +
    box.height +
    "px;";
  return s;
}

function getRectStyles(range: Range, x: number, y: number): [number, string[]] {
  const rects = [];
  const clientRects = range.getClientRects();
  let minY = Number.MAX_SAFE_INTEGER;
  for (let i = 0; i < clientRects.length; ++i) {
    const r = clientRects[i];
    if (r.width && r.height) {
      r.x -= x;
      r.y -= y;
      minY = Math.min(minY, r.y);
      rects.push(style(r));
    }
  }
  return [minY, rects];
}

async function selectionChangeHandler(
  pdf: Pdf,
  selectionListener: (text?: SelectionTexts) => void
) {
  if (!pdf.pageView.textLayer) {
    return;
  }
  const selectedTexts = getSelectedText(
    pdf.pageNumber,
    pdf.pageView.textLayer.textLayerDiv
  );
  if (!selectedTexts) {
    return;
  }
  if (!selectedTexts.texts) {
    selectionListener();
    return;
  }
  if (selectedTexts.texts) {
    selectionListener(selectedTexts);
  } else {
    selectionListener();
  }
}

export function setSelectionListener(
  pdf: Pdf,
  selectionListener: (text?: SelectionTexts) => void
) {
  document.addEventListener("selectionchange", async () => {
    await selectionChangeHandler(pdf, selectionListener);
  });
}

function addEols(
  text: string[],
  offset: number,
  texts: (TextItem | TextMarkedContent)[]
): string[] {
  const out = [];
  for (let i = 0; i < text.length; i += 1) {
    let item = texts[offset + i];
    while (!("str" in item) || item.str.length === 0) {
      offset += 1;
      item = texts[offset + i];
    }
    let s = item.str;
    if (item.hasEOL && i !== text.length - 1) {
      s += " ";
    }
    out.push(s);
  }
  return out;
}

/// find the text in the pdf page text
function findText(
  needle: string[],
  hay: (TextItem | TextMarkedContent)[]
): string[] | undefined {
  if (needle.length === 0 || needle.length > hay.length) {
    return;
  }
  for (let i = 0; i < hay.length - needle.length + 1; i += 1) {
    let match = true;
    let j = 0;
    let k = i;
    while (match && j < needle.length && k < hay.length) {
      const item = hay[k];
      if ("str" in item && item.str.length > 0) {
        match = needle[j] === item.str;
      } else {
        k += 1;
      }
      j += 1;
      k += 1;
    }
    if (match) {
      return addEols(needle, i, hay);
    }
  }
}

export type PageTextIndex = {
  texts: Text[];
  textStrings: string[];
  fullText: string;
  div: HTMLDivElement;
};

export function createPageTextIndex(div: HTMLDivElement): PageTextIndex {
  const { texts, textStrings } = getTexts(div);
  const fullText = textStrings.join("");
  return {
    texts,
    textStrings,
    fullText,
    div,
  };
}

export function findHighlightRects(
  pageNumber: number,
  index: PageTextIndex,
  textQuote: RefinedBy
): SelectionTexts | undefined {
  const { texts, textStrings, fullText, div } = index;
  const prefix = textQuote.prefix || "";
  const suffix = textQuote.suffix || "";
  const frag = prefix + textQuote.exact + suffix;
  let pos = fullText.indexOf(frag);
  if (pos === -1) {
    // the textQuote was not found on this page
    return;
  }
  pos += prefix.length;
  let offset = 0;
  const endPos = pos + textQuote.exact.length;
  let startNode = null;
  let endNode = null;
  let startOffset = 0;
  let endOffset = 0;
  let left = textQuote.exact.length;
  const annotationTextStrings = [];
  let i = 0;
  while (i < textStrings.length && offset < endPos) {
    const text = textStrings[i];
    const textLength = text.length;
    if (offset + textLength > pos) {
      annotationTextStrings.push(text);
      endNode = texts[i];
      endOffset = left;
      if (!startNode) {
        startNode = endNode;
        startOffset = pos - offset;
        endOffset = startOffset + left;
        left += startOffset;
      }
      left -= textLength;
    }
    offset += textLength;
    i += 1;
  }
  if (!startNode || !endNode) {
    return;
  }
  const range = document.createRange();
  range.setStart(startNode, startOffset);
  range.setEnd(endNode, endOffset);
  const nodeCount = i;
  const id = pageNumber + " " + nodeCount + " " + textQuote.exact;
  return createSelectionTexts(id, div, range, annotationTextStrings);
}

function createSelectionTexts(
  id: string,
  div: HTMLDivElement,
  range: Range,
  texts: string[]
): SelectionTexts {
  const divRect = div.getClientRects()[0];
  const [y, rectStyles] = getRectStyles(range, divRect.x, divRect.y);
  return {
    id,
    startOffset: range.startOffset,
    endOffset: range.endOffset,
    rectStyles,
    y,
    texts,
  };
}
