// SPDX-FileCopyrightText: 2022 Jos van den Oever
//
// SPDX-License-Identifier: AGPL-3.0-only

import { SelectionTexts } from "./pdf";
import { PdfAnnotationSchema } from "./pdf-annotation";

// this type is a mix of runtime information and the web annotation
export type Annotation = {
  // a unique number for vue lists
  id: number;
  // the Web Annotation subset
  annotation: PdfAnnotationSchema;
  // The highlighting rectangles for the annotation text.
  // This can be undefined if the page of this annotation has not
  // been shown yet.
  selection: SelectionTexts | undefined;
  // The page number derived from `target.selector.value`.
  page: number;
};

export type PageSelection = {
  pdfUrl: string;
  page: number;
  selection?: SelectionTexts;
};

export function emptyPageSelection(): PageSelection {
  return {
    pdfUrl: "",
    page: 1,
  };
}
