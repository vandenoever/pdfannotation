// SPDX-FileCopyrightText: 2022 Jos van den Oever
//
// SPDX-License-Identifier: AGPL-3.0-only

import { defineStore } from "pinia";
import { Annotation } from "../Annotation";
import { Convert } from "../pdf-annotation";

type RootState = {
  annotations: Annotation[];
  hoveredAnnotation: Annotation | undefined;
  nextAnnotationId: number;
  userUrl: string;
};

export const useAnnotationsStore = defineStore({
  id: "annotations",
  state: () => {
    const state = {
      annotations: [],
      hoveredAnnotation: undefined,
      nextAnnotationId: 0,
      userUrl: "",
    } as RootState;
    return state;
  },
  actions: {
    setHovered(annotation: Annotation | undefined) {
      this.hoveredAnnotation = annotation;
    },
    deleteAnnotation(annotation: Annotation) {
      const index = this.annotations.indexOf(annotation);
      if (index !== -1) {
        this.annotations.splice(index, 1);
      }
      if (this.hoveredAnnotation === annotation) {
        this.hoveredAnnotation = undefined;
      }
      save(this.annotations);
    },
    storeAnnotation(annotation: Annotation) {
      const index = this.annotations.findIndex((b) => annotation.id === b.id);
      if (index !== -1) {
        this.annotations[index] = annotation;
      }
      if (this.hoveredAnnotation === annotation) {
        this.hoveredAnnotation = annotation;
      }
      save(this.annotations);
    },
    addAnnotation(annotation: Annotation) {
      this.annotations.push(annotation);
      this.annotations.sort((a, b) => {
        if (a.page === b.page) {
          if (a.selection && b.selection) {
            return a.selection.y - b.selection.y;
          }
        }
        return a.page - b.page;
      });
      save(this.annotations);
    },
    async loadAnnotations() {
      this.annotations = await load(this);
    },
    getNextAnnotationId(): number {
      const next = this.nextAnnotationId;
      this.nextAnnotationId += 1;
      return next;
    },
  },
});

const pageRegex = /^page=([1-9][0-9]*)$/;

function getPage(fragment: string): number {
  const m = pageRegex.exec(fragment);
  if (m) {
    return parseInt(m[1]);
  } else {
    throw Error(`Could not parse ${fragment}`);
  }
}

async function load(store: {
  getNextAnnotationId(): number;
}): Promise<Annotation[]> {
  const result = await fetch("/files/annotations.json");
  const data = await result.json();
  const annotations = Convert.toPdfAnnotation(data);
  const as = [];
  for (const annotation of annotations.pdfAnnotations) {
    const page = getPage(annotation.target.selector.value);
    as.push({
      id: store.getNextAnnotationId(),
      annotation,
      page,
      selection: undefined,
    });
  }
  return as;
}

async function save(annotations: Annotation[]) {
  const out = [];
  for (const annotation of annotations) {
    out.push(annotation.annotation);
  }
  console.log(out);
  const json = Convert.pdfAnnotationToJson({ pdfAnnotations: out });
  const result = await fetch("/files/save.json", {
    method: "PUT",
    body: JSON.stringify(json, null, "  "),
  });
  console.log(result);
}
