// SPDX-FileCopyrightText: 2022 Jos van den Oever
//
// SPDX-License-Identifier: AGPL-3.0-only

import {
  BodyFormat,
  BodyType,
  ConformsTo,
  Context,
  PdfAnnotationSchema,
  PdfAnnotationType,
  RefinedBy,
  RefinedByType,
  SelectorType,
  TargetFormat,
} from "./pdf-annotation";
import { v4 as uuidv4 } from "uuid";

export function createPdfAnnotation(
  targetId: string,
  creator: string,
  page: number,
  textQuote: RefinedBy
): PdfAnnotationSchema {
  return {
    context: Context.HttpWwwW3OrgNsAnnoJsonld,
    type: PdfAnnotationType.Annotation,
    id: uuidv4(),
    creator,
    created: new Date(),
    body: {
      format: BodyFormat.TextPlain,
      type: BodyType.TextualBody,
      value: "",
    },
    target: {
      format: TargetFormat.ApplicationPdf,
      id: targetId,
      selector: {
        type: SelectorType.FragmentSelector,
        conformsTo: ConformsTo.HttpToolsIetfOrgRfcRfc3778,
        value: `page=${page}`,
        refinedBy: textQuote,
      },
    },
  };
}
